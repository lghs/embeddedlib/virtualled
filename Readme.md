# neopixel

library used to add an abstraction layer to use neopixel

to be used with `fastled/FastLED@^3.5.0`

```¢pp

CRGB LED[NUM_LEDS];
VirtualLed virtualLed(0, 10);
BlinkLedColorPattern redFastPattern(RED, FAST);

void setup() {
    FastLED.setBrightness(10);
    FastLED.addLeds<WS2812B, LED_PIN, GRB>(LED, NUM_LEDS);
    virtualLed.setPattern(redFastPattern);

}

void loop() {
    virtualLed.applyColor(LED);
    FastLED.show();
}


```
