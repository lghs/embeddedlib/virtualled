#pragma once

#define RED CRGB(255,0,0)
#define GREEN CRGB(0,255,0)
#define BLUE CRGB(0,0,255)
#define SLOW 500
#define FAST 200
#define SUPERFAST 100

const SolidLedColorPattern SOLID_BLACK(BLACK);