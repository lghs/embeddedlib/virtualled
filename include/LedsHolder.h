#pragma once

#include <FastLED.h>
/**
 * an interface to attribute colors to leds
 */
class LedsHolder{
public:
    virtual void applyColor(CRGB led[]);
};