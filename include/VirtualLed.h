#pragma once


#include <pattern/SolidLedColorPattern.h>
#include "LedsHolder.h"
#include "pattern/LedColorPattern.h"
#include "LedUtils.h"

class  VirtualLed : public LedsHolder {
public:
    VirtualLed(unsigned int startIndex,
               unsigned int numberOfLeds = 0,
               const LedColorPattern *initPattern = &BLACK
    );

    LedColorPattern* defaultPattern = (LedColorPattern *) &SOLID_BLACK;
    /**
     * set a pattern for a duration of time
     * @param ledColorPattern
     * @param duration if 0 set the pattern indefinitely
     */
    void setPattern(LedColorPattern *ledColorPattern, unsigned long duration = 0, bool thenComeBackToCurrent=false);

    LedColorPattern* getPattern();

    void applyColor(CRGB led[]);

private:
    unsigned int startIndex;
    unsigned int numberOfLed;
    LedColorPattern *fallBackPattern;
    LedColorPattern *pattern;
    unsigned long lastChange = 0;
    unsigned long duration = 0;
};



