#pragma once


#include "LedColorPattern.h"

class BlinkLedColorPattern : public LedColorPattern {
public:
    BlinkLedColorPattern(CRGB color, unsigned long periodMs, CRGB otherColor = CRGB(0, 0, 0));

    const CRGB ledColor(unsigned long at) override;

    String descr() override {
        return String("blink led color pattern");
    }

private:
    CRGB color;
    unsigned long periodMs;
    CRGB otherColor;

};


