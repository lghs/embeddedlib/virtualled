#pragma once

#include <FastLED.h>

/**
 * a color pattern, for exemple blink red or ramping green
 */
class LedColorPattern {
public:
    const virtual CRGB ledColor(unsigned long at) = 0;

    virtual String descr() {
        return String("Abstract led color pattern");
    }
};
