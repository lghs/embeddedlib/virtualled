#pragma once


#include "LedColorPattern.h"

class SolidLedColorPattern : public LedColorPattern{
public:
    SolidLedColorPattern(CRGB color);
    const virtual CRGB ledColor(unsigned long at);

    String descr() override{
        return String("solid led color pattern");
    }
private:
    CRGB _color;
};

static const SolidLedColorPattern BLACK = SolidLedColorPattern(CRGB(0,0,0));

