

#include "VirtualLed.h"

VirtualLed::VirtualLed(unsigned int startIndex, unsigned int numberOfLeds, const LedColorPattern *pattern)
        : startIndex(startIndex), numberOfLed(numberOfLeds), pattern(const_cast<LedColorPattern *>(pattern)) {
    lastChange = millis();
}

void VirtualLed::setPattern(LedColorPattern *ledColorPattern, unsigned long p_duration, bool thenComeBackToCurrent) {
    if (pattern != ledColorPattern) {
        if (thenComeBackToCurrent) {
            fallBackPattern = pattern;
        } else {
            fallBackPattern = defaultPattern;
        }
        pattern = ledColorPattern;
        lastChange = millis();
        duration = p_duration;
    } else if (p_duration == 0) {
        duration = 0;
    } else {
        duration = (millis() - lastChange) + p_duration;
    }
}

void VirtualLed::applyColor(CRGB *led) {
    bool patternNotExpired = duration == 0 || (millis() - lastChange) < duration;
    bool inPattern = (pattern) && patternNotExpired;
    bool inFallbackPattern = !inPattern && fallBackPattern;
    CRGB color;
    if (inPattern) {
        color = pattern->ledColor(millis() - lastChange);
    } else if (pattern && !patternNotExpired && fallBackPattern) {
        color = fallBackPattern->ledColor(millis() - lastChange);
    } else {
        color = defaultPattern->ledColor(millis() - lastChange);
    }
    for (int i = startIndex; i < (startIndex + numberOfLed); i++) {
        led[i] = color;
    }
}

LedColorPattern *VirtualLed::getPattern() {
    return pattern;
}