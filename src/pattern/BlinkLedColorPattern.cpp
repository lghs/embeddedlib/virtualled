#include "pattern/BlinkLedColorPattern.h"

BlinkLedColorPattern::BlinkLedColorPattern(CRGB color,
                                           unsigned long periodMs,
                                           CRGB otherColor) : color(color),
                                                              periodMs(periodMs),
                                                              otherColor(otherColor) {
}

const CRGB BlinkLedColorPattern::ledColor(unsigned long at) {
    if (((at / periodMs) % 2) == 0) {
        return color;
    } else {
        return otherColor;
    }
}
