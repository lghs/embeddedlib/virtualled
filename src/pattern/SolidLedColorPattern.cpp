#include "pattern/SolidLedColorPattern.h"

SolidLedColorPattern::SolidLedColorPattern(CRGB color) {
    _color = color;
}

const CRGB SolidLedColorPattern::ledColor(unsigned long at) {
    return _color;
}
